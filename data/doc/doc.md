-dataset
	Folder contains files with pickled lists of game states containing board object (from server folder), number of player on turn and maximum possible transfers of that player.
-dataset/game_info
	Folder contatins files with pickled list with players order and then number of the winning player.
*Corresponding files in data and data/game_info have same names. For loading from game states I recommend yield syntax as in https://stackoverflow.com/questions/20716812/saving-and-loading-multiple-objects-in-pickle-file.
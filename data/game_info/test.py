import pickle

def p_load(f):
	try:
		while True:
			yield pickle.load(f)
	except EOFError:
		pass

if __name__ == "__main__":
	with open("../0bc823c47ebc4915a67682ed1a9029a2", 'rb') as f:
		for x in p_load(f):
			print(type(x))